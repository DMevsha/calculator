<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="Audition" class="base.calculator.model.audition.Audition" scope="request"></jsp:useBean>
<jsp:setProperty name="Audition" property="*"></jsp:setProperty>
<html>
<head>
    <title>Audition</title>
</head>
<body>
<h4>Fields marked with * are impotent!</h4>
<form action="/result" method="post">

        <pre>
            First Name*

            <input type="text" name="firstName" value="" size=15 maxlength=20>

            Last Name*

            <input type="text" name="lastName" value="" size=15 maxlength=20>

            E-Mail*

            <input type="text" name="email" value="" size=25  maxlength=125>

            Why you use this Calculator?

            <input type="text" name="explaining" value="" size=25  maxlength=125>

            how old are you?

            <select name="age">
                <option>18</option>
                <option>19</option>
                <option>20</option>
                <option>21</option>
                <option>22</option>
                <option>23</option>
                <option>24</option>
                <option>25</option>
                <option>26</option>
                <option>27</option>
                <option>28</option>
                <option>29</option>
            </select>

            What music are you interested in?

             <input type="checkbox" name="faveMusic" value="Rock">Rock
             <input type="checkbox" name="faveMusic" value="Pop">Pop
             <input type="checkbox" name="faveMusic" value="Bluegrass">Bluegrass
             <input type="checkbox" name="faveMusic" value="Blues">Blues
             <input type="checkbox" name="faveMusic" value="Jazz">Jazz
             <input type="checkbox" name="faveMusic" value="Country">Country

             Would you like to receive e-mail notifications on our special
             sales?

             <input type="radio" name="notify" value="Yes" checked>Yes

             <input type="radio" name="notify" value="No" > No

             <input type="submit" value="Submit"> <input type="reset"
                                                                value="Reset">

        </pre>
</form>
</body>
</html>