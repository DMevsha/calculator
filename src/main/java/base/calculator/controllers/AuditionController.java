package base.calculator.controllers;

import base.calculator.model.audition.Audition;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Controller
public class AuditionController {
    @RequestMapping("/audition")
    public String goTo(){
        return "content/audition";
    }
    @RequestMapping("/result")
    public String getResult(Audition audition, ModelMap map, HttpServletResponse response){

        if (audition.validate()){
            map.put("message", "success");
            map.put("a", audition);
            Cookie cookie = new Cookie("Audition", "complete");
            response.addCookie(cookie);
            return "content/result";
        }else{
            map.put("message", "unSuccess");
            map.put("a", audition);
            return "content/retry";
        }
    }
}
