package base.calculator.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class MainController {

    @RequestMapping(method = RequestMethod.GET)
    public String start(Model model){
        System.err.println("mainController invoked");
        return "index";
    }
@RequestMapping("content")
    public String content(Model model){
        System.err.println("mainController invoked");
        return "content/calculator";
}


}
