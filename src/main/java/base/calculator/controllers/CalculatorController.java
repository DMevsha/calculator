package base.calculator.controllers;

import base.calculator.model.Calculation;
import base.calculator.model.InputNumbers;
import base.calculator.model.service.JSonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
public class CalculatorController {

    @Autowired
    InputNumbers inputNumbers;

    @Autowired
    Calculation calculation;

    @RequestMapping("/calculate")
    public //@ResponseBody
    JSonResponse doCalculation(HttpSession session, @RequestParam("action") String action){
        JSonResponse response;

        String result = "";
        if (session.getAttribute("result") != null){
            result = (String) session.getAttribute("result");
        }

        String value = "";
        if (session.getAttribute("value") != null){
            value = (String) session.getAttribute("value");
        }

        if (action.equals("C")){
            session.removeAttribute("result");
            session.removeAttribute("lastAction");
            response = new JSonResponse("","",value);

            return response;
        }

        String lastAction = (String) session.getAttribute("lastAction");


        if (lastAction != null && !result.equals("") && !value.equals("")){
            result = calculation.calculate(Double.parseDouble(result), lastAction, Double.parseDouble(value));
            value = "";

        }
        if (action.equals("=")){
            if (result.equals("")){
                response = new JSonResponse("", "", value);
                session.setAttribute("value", value);
            }else{
                session.setAttribute("result", result);
                session.removeAttribute("value");
                session.removeAttribute("lastAction");
                response = new JSonResponse(result, "", "");
            }

            return response;
        }

        if (!value.equals("")){
            result = value;
        }
        session.removeAttribute("value");
        session.setAttribute("lastAction", action);
        session.setAttribute("result", result);

        response = new JSonResponse(result, action, "");
        return response;
    }
    @RequestMapping("/input")
    public //@ResponseBody
    String input(@RequestParam("element") String element, HttpSession session){

        String value = (String) session.getAttribute("value");
        String result;

        if (value == null){
            result = element;
        }else {
            System.out.println("element: " + element + " value: " + value);
            if (value.length() > 7){
                if (element.equals("clean")){
                    result = "";
                }else{
                    result = value;
                }
            }else{
                result = inputNumbers.input(value, element);
            }
        }

        session.setAttribute("value", result);
        return result;
    }

}
