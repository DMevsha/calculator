package base.calculator.model.service;


public class JSonResponse {
    private String result;
    private String lastAction;
    private  String value;

    public JSonResponse(String result, String lastAction, String value) {
        this.result = result;
        this.lastAction = lastAction;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getLastAction() {
        return lastAction;
    }

    public void setLastAction(String lastAction) {
        this.lastAction = lastAction;
    }

    public String toString(){
        return "result: " + result + " lastAction: " + lastAction + " value: " + value;
    }
}
