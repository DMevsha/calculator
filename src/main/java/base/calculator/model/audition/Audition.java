package base.calculator.model.audition;

import java.util.Hashtable;

public class Audition {

    String firstName;
    String lastName;
    String email;
    String age;
    String explaining;
    String[] faveMusic;
    Hashtable errors;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Audition() {
        firstName = "";
        lastName = "";
        email = "";
        explaining = "";
        age = "";
        faveMusic = new String[] { "1" };
        errors = new Hashtable();
    }



    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Hashtable getErrors() {
        return errors;
    }

    public void setErrors(Hashtable errors) {
        this.errors = errors;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExplaining() {
        return explaining;
    }

    public void setExplaining(String explaining) {
        this.explaining = explaining;
    }

    public String[] getFaveMusic() {
        return faveMusic;
    }

    public void setFaveMusic(String[] faveMusic) {
        this.faveMusic = faveMusic;
    }

    public boolean validate() {
        boolean allOk = true;
        if (firstName.equals("")){
            allOk = false;
            errors.put("firstName", "please enter your first name");
        }if (lastName.equals("")){
            allOk = false;
            errors.put("lastName", "please enter your last name");
        }if (email.equals("")){
            allOk = false;
            errors.put("email", "please enter your email");
        }if (explaining.equals("")){
            allOk = false;
            errors.put("explaining", "please enter why you use this calculator");
        }
        return allOk;
    }

    public String getErrorMsg(String s) {
        String errorMsg =(String)errors.get(s.trim());
        return (errorMsg == null) ? "":errorMsg;
    }

    public String isFavMusicSelected(String s) {
        boolean found=false;
        if (!faveMusic[0].equals("1")) {
            for (int i = 0; i < faveMusic.length; i++) {
                if (faveMusic[i].equals(s)) {
                    found=true;
                    break;
                }
            }
            if (found) return "checked";
        }
        return "";
    }

}
