package base.calculator.model;


import org.springframework.stereotype.Service;


@Service
public class Calculation {

    public String calculate(double result, String action, double value){

        switch (action){
            case "+":
                result += value;
                break;
            case "/":
                result /= value;
                break;
            case "-":
                result -= value;
                break;
            case "*":
                result *= value;
                break;
            case "=":
                break;
        }

        return String.valueOf(result);
    }
}
