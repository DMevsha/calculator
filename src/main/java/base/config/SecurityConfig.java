package base.config;

import base.secure.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    public void registerGlobalAuthentication(@SuppressWarnings("SpringJavaAutowiringInspection") AuthenticationManagerBuilder auth) throws Exception {
        auth
                //inMemoryAuthentication()
                //.withUser("user").password("1").roles("USER").and()
                //.withUser("admin").password("2").roles("ADMIN");
                .userDetailsService(userDetailsService);
                //.passwordEncoder(getShaPasswordEncoder());
    }



    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf()
                .disable()
                        // указываем правила запросов
                        // по которым будет определятся доступ к ресурсам и остальным данным
                .authorizeRequests()
                .antMatchers("/resources/**").permitAll();

        http
                .authorizeRequests().anyRequest().permitAll()
                .antMatchers("/").permitAll()
                .anyRequest().authenticated();


        http
                .formLogin()
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/content/**").hasRole("ADMIN")
                .antMatchers("/content/**").hasRole("USER")
                .antMatchers("/checkout").hasRole("USER");







        /*http.formLogin()
                // указываем страницу с формой логина

                // указываем action с формы логина
                .loginProcessingUrl("/j_spring_security_check")
                        // указываем URL при неудачном логине
                .failureUrl("/login?error")
                        // Указываем параметры логина и пароля с формы логина
                .usernameParameter("j_username")
                .passwordParameter("j_password")
                        // даем доступ к форме логина всем
                .permitAll();*/

        http.logout()
                // разрешаем делать логаут всем
                .permitAll()
                        // указываем URL логаута
                .logoutUrl("/logout")
                        // указываем URL при удачном логауте
                .logoutSuccessUrl("/")
                        // делаем не валидной текущую сессию
                .invalidateHttpSession(true);

        /*http
                .formLogin()
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/app/**").hasRole("ADMIN")
                .antMatchers("/order/**").hasRole("USER")
                .antMatchers("/checkout").hasRole("USER")
                .anyRequest().anonymous();*/



                /*        //This will generate a login form if none is supplied.
                .formLogin();*/


               /* .formLogin()
                .loginPage("/index.jsp")
                .defaultSuccessUrl("/app/")
                .failureUrl("/index.jsp")
                .permitAll()
                .and()
                .logout()
                .logoutSuccessUrl("/index.jsp");*/

                /*.antMatchers("/order/**").hasRole("USER")
                .antMatchers("/checkout").hasRole("USER")
                .anyRequest().anonymous()
                .and()
                        //This will generate a login form if none is supplied.
                .formLogin();*/



       /* http

                .formLogin()
                .defaultSuccessUrl("/hello")
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/logout")
                .permitAll();
        http
                .sessionManagement()
                .maximumSessions(1)
                .maxSessionsPreventsLogin(true);*/

    }


    /*@Bean
    public ShaPasswordEncoder getShaPasswordEncoder(){
        return new ShaPasswordEncoder();
    }*/

}