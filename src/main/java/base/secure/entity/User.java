package base.secure.entity;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.HashSet;
import java.util.Set;

public class User {

    private String login;
    private String password;
    private Set<SimpleGrantedAuthority> roles;

    public User(String login, String password, Set<SimpleGrantedAuthority> roles) {
        this.login = login;
        this.password = password;
        this.roles = roles;
    }

    public User() {
        roles = new HashSet<>();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<SimpleGrantedAuthority> getRoles() { return roles; }

    public void setRoles(Set<SimpleGrantedAuthority> role) { this.roles = role; }
}
