package base.secure.DAO;


import base.secure.DAO.entity.UserEntity;

public interface DAO {
    public UserEntity getUserByLogin(String login);
}
