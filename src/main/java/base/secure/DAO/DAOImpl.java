package base.secure.DAO;

import base.secure.DAO.entity.UserEntity;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Repository;




@Repository
public class DAOImpl implements DAO {

    @Autowired
    SessionFactory sessionFactory;

    private synchronized Session getSession(){
        Session session = sessionFactory.getCurrentSession();
        if (session == null){
            session = sessionFactory.openSession();
        }
        return session;
    }

    @SuppressWarnings("JpaQlInspection")
    @Override
    public UserEntity getUserByLogin(String login) {
        System.err.println("getting User with login: " + login + " started");
        Session session = getSession();
        Query query = session.createQuery("from UserEntity u where u.login like :login");
        query.setParameter("login", login);
        UserEntity userEntity = (UserEntity) query.uniqueResult();
        System.err.println("user login: " + userEntity.getLogin() + " use password: " + userEntity.getPassword()
                         + " user role: " + userEntity.getRole());

        if (userEntity == null){
            throw new BadCredentialsException("Bad Credentials");
        }
        return userEntity;
    }
}
