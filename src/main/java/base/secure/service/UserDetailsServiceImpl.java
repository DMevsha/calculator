package base.secure.service;

import base.secure.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = userService.getUser(userName);

        UserDetails userDetails =
                new org.springframework.security.core.userdetails.User(  user.getLogin(),
                                                                         user.getPassword(),
                                                                         user.getRoles()    );

        return userDetails;
    }

}
