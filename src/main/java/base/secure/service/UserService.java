package base.secure.service;

import base.secure.entity.User;

public interface UserService {

    User getUser(String login);

}
