package base.secure.service;

import base.secure.DAO.DAO;
import base.secure.DAO.entity.UserEntity;
import base.secure.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    DAO dao;

    @Override
    @Transactional
    public User getUser(String login) {
        User user = new User();
        UserEntity userEntity = dao.getUserByLogin(login);
        if (userEntity.getLogin() == null ){
            throw new BadCredentialsException("Bad Credentials");
        }
        user.setLogin( userEntity.getLogin() );
        user.setPassword( userEntity.getPassword() );
        user.getRoles().add( new SimpleGrantedAuthority( userEntity.getRole() ) );

        return user;
    }

}
